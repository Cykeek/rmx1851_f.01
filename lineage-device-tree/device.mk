#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := nosdcard

# Rootdir
PRODUCT_PACKAGES += \
    init.qcom.sh \
    init.qcom.efs.sync.sh \
    init.qcom.early_boot.sh \
    init.mdm.sh \
    init.qti.chg_policy.sh \
    install-recovery.sh \
    init.qcom.sdio.sh \
    init.qti.qcv.sh \
    qca6234-service.sh \
    init.qcom.usb.sh \
    init.class_main.sh \
    init.qcom.sensors.sh \
    init.crda.sh \
    init.qcom.coex.sh \
    init.qcom.post_boot.sh \
    init.at.class_main.sh \
    init.qcom.class_core.sh \
    init.at.post_boot.sh \

PRODUCT_PACKAGES += \
    fstab.at.qcom \
    init.qti.ufs.rc \
    init.at.qcom.rc \
    init.target.rc \
    init.qcom.usb.rc \
    init.oppo.vendor.motor.rc \
    init.oppo.sensor.rc \
    init.wlan.target.rc \
    init.qcom.rc \
    init.at.target.rc \
    init.wlan.qcom.rc \
    init.qcom.factory.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/qualcomm/qssi/qssi-vendor.mk)
